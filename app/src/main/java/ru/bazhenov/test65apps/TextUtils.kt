package ru.bazhenov.test65apps

class TextUtils {

    companion object {

        fun capitalize(string : String?) : String {
            val s = if (string != null) string.toLowerCase() else ""
            return Character.toUpperCase(s[0]) + s.substring(1)
        }
    }
}