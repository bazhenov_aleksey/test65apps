package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ru.bazhenov.test65apps.DateUtils
import ru.bazhenov.test65apps.TextUtils
import ru.bazhenov.test65apps.model.api.ServerResponse
import java.io.Serializable

@Entity
class Employee : Serializable {

    @PrimaryKey
    var id: Int = 0
    var firstName: String? = null
    var lastName: String? = null
    var birthday: String? = null
    var avatarUrl: String? = null
    var age: String? = null

    companion object {

        fun convertToDbObjects(employees: List<ServerResponse.EmployeeResponse>?): ArrayList<Employee> {
            val list = ArrayList<Employee>()
            for ((index, e) in employees!!.withIndex()) {
                with(e) {
                    val employee = Employee()
                    employee.id = index
                    employee.firstName = TextUtils.capitalize(firstName)
                    employee.lastName = TextUtils.capitalize(lastName)
                    employee.birthday = birthday
                    employee.avatarUrl = avatarUrl
                    employee.age = DateUtils.getAge(birthday)
                    list.add(employee)
                }
            }
            return list
        }
    }
}