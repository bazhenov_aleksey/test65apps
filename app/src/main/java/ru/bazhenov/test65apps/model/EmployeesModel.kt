package ru.bazhenov.test65apps.model

import io.reactivex.Flowable
import ru.bazhenov.test65apps.model.db.Employee

interface EmployeesModel {

    fun getEmployees(specialtyId: Int): Flowable<List<Employee>>
}
