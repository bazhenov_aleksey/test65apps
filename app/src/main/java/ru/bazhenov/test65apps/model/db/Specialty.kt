package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ru.bazhenov.test65apps.model.api.ServerResponse

@Entity
class Specialty {

    @PrimaryKey
    var id: Int = 0
    var name: String? = null

    companion object {

        fun convertToDbObjects(employees: List<ServerResponse.EmployeeResponse>?): ArrayList<Specialty> {
            val list = ArrayList<Specialty>()
            for (e in employees!!) {
                for (s in e.specialty!!) {
                    val speciality = Specialty()
                    speciality.id = s.id!!
                    speciality.name = s.name
                    list.add(speciality)
                }
            }
            return list
        }
    }
}