package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface EmployeeSpecialtyJoinDao {

    @Insert
    fun insert(join: List<EmployeeSpecialtyJoin>)

    @Query("SELECT * FROM Employee INNER JOIN EmployeeSpecialtyJoin " +
            "ON Employee.id=EmployeeSpecialtyJoin.employeeId " +
            "WHERE EmployeeSpecialtyJoin.specialtyId=:specialtyId")
    fun getEmployeesForSpecialty(specialtyId: Int): Flowable<List<Employee>>

    @Query("SELECT * FROM Specialty INNER JOIN EmployeeSpecialtyJoin " +
            "ON Specialty.id=EmployeeSpecialtyJoin.specialtyId " +
            "WHERE EmployeeSpecialtyJoin.employeeId=:employeeId")
    fun getSpecialtiesForEmployee(employeeId: Int): Flowable<List<Specialty>>

    @Query("DELETE FROM EmployeeSpecialtyJoin")
    fun deleteAll()
}
