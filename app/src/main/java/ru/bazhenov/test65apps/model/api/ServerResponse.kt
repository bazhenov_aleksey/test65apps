package ru.bazhenov.test65apps.model.api

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ServerResponse {

    @SerializedName("response")
    val employees: List<EmployeeResponse>? = null

    class EmployeeResponse : Serializable {
        var id: Int? = null
        @SerializedName("f_name")
        val firstName: String? = null
        @SerializedName("l_name")
        val lastName: String? = null
        val birthday: String? = null
        @SerializedName("avatr_url")
        val avatarUrl: String? = null
        val specialty: List<SpecialtyResponse>? = null
    }

    class SpecialtyResponse {
        @SerializedName("specialty_id")
        val id: Int? = null
        val name: String? = null
    }
}