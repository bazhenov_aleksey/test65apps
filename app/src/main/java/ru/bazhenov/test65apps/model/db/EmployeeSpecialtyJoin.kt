package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import ru.bazhenov.test65apps.model.api.ServerResponse
import java.io.Serializable

@Entity(primaryKeys = arrayOf("employeeId", "specialtyId"),
        foreignKeys = arrayOf(
                ForeignKey(
                        entity = Employee::class,
                        parentColumns = arrayOf("id"),
                        childColumns = arrayOf("employeeId"),
                        onDelete = CASCADE),
                ForeignKey(
                        entity = Specialty::class,
                        parentColumns = arrayOf("id"),
                        childColumns = arrayOf("specialtyId"),
                        onDelete = CASCADE)
        )
)

class EmployeeSpecialtyJoin(val employeeId: Int, val specialtyId: Int) : Serializable {
    companion object {

        fun convertToDbObjects(employees: List<ServerResponse.EmployeeResponse>?): ArrayList<EmployeeSpecialtyJoin> {
            val list = ArrayList<EmployeeSpecialtyJoin>()
            for (e in employees!!) {
                for (s in e.specialty!!) {
                    val emplSpec = EmployeeSpecialtyJoin(e.id!!, s.id!!)
                    list.add(emplSpec)
                }
            }
            return list
        }
    }
}