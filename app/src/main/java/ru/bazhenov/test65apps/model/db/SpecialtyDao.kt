package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface SpecialtyDao {

    @Query("SELECT * FROM Specialty")
    fun loadAll(): Flowable<List<Specialty>>

    @Query("SELECT * FROM Specialty WHERE id IN (:ids)")
    fun loadByIds(ids: IntArray): List<Specialty>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(specialities: List<Specialty>)

    @Query("DELETE FROM Specialty")
    fun deleteAll()
}

