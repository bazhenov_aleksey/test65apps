package ru.bazhenov.test65apps.model.api

import io.reactivex.Observable
import retrofit2.http.GET

interface ApiInterface {

    @get:GET("static/raw/master/testTask.json")
    val loadData: Observable<ServerResponse>
}
