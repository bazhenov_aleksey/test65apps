package ru.bazhenov.test65apps.model

import io.reactivex.Flowable
import ru.bazhenov.test65apps.App
import ru.bazhenov.test65apps.model.db.AppDatabase
import ru.bazhenov.test65apps.model.db.Employee
import javax.inject.Inject

class EmployeesModelImpl : EmployeesModel {

    @Inject
    lateinit var db: AppDatabase

    init {
        App.dbComponent.inject(this)
    }

    override fun getEmployees(specialtyId: Int): Flowable<List<Employee>> {
        return db.employeeSpecialtyDao().getEmployeesForSpecialty(specialtyId)
    }
}