package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(Employee::class, Specialty::class, EmployeeSpecialtyJoin::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun employeeDao(): EmployeeDao
    abstract fun specialityDao(): SpecialtyDao
    abstract fun employeeSpecialtyDao(): EmployeeSpecialtyJoinDao

}
