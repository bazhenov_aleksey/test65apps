package ru.bazhenov.test65apps.model.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface EmployeeDao {

    @Query("SELECT * FROM Employee WHERE id=:employeeId LIMIT 1")
    fun loadById(employeeId: Int): Flowable<Employee>

    @Insert
    fun insertAll(users: List<Employee>)

    @Query("DELETE FROM Employee")
    fun deleteAll()
}

