package ru.bazhenov.test65apps.model

import io.reactivex.Flowable
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.model.db.Specialty

interface EmployeeDetailsModel {

    fun getEmployeeDetails(employeeId: Int): Flowable<Employee>
    fun getEmployeeSpecialties(employeeId: Int): Flowable<List<Specialty>>
}
