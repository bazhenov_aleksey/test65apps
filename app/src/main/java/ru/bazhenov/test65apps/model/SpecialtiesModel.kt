package ru.bazhenov.test65apps.model

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import ru.bazhenov.test65apps.model.api.ServerResponse
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.model.db.EmployeeSpecialtyJoin
import ru.bazhenov.test65apps.model.db.Specialty

interface SpecialtiesModel {

    fun getDataFromServer(): Observable<ServerResponse>

    fun insertDataToDb(
            employees: ArrayList<Employee>,
            specialities: ArrayList<Specialty>,
            employeeSpecialtyJoin: ArrayList<EmployeeSpecialtyJoin>): Completable

    fun getSpecialtiesFromDb(): Flowable<List<Specialty>>
}
