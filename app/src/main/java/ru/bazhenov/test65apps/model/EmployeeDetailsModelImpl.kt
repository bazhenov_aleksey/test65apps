package ru.bazhenov.test65apps.model

import io.reactivex.Flowable
import ru.bazhenov.test65apps.App
import ru.bazhenov.test65apps.model.db.AppDatabase
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.model.db.Specialty
import javax.inject.Inject

class EmployeeDetailsModelImpl : EmployeeDetailsModel {

    @Inject
    lateinit var db: AppDatabase

    init {
        App.dbComponent.inject(this)
    }

    override fun getEmployeeDetails(employeeId: Int): Flowable<Employee> {
        return db.employeeDao().loadById(employeeId)
    }

    override fun getEmployeeSpecialties(employeeId: Int): Flowable<List<Specialty>> {
        return db.employeeSpecialtyDao().getSpecialtiesForEmployee(employeeId)
    }
}