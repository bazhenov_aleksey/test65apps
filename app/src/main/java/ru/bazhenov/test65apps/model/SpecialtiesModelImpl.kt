package ru.bazhenov.test65apps.model

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import ru.bazhenov.test65apps.App
import ru.bazhenov.test65apps.model.api.ApiService
import ru.bazhenov.test65apps.model.api.ServerResponse
import ru.bazhenov.test65apps.model.db.AppDatabase
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.model.db.EmployeeSpecialtyJoin
import ru.bazhenov.test65apps.model.db.Specialty
import javax.inject.Inject

class SpecialtiesModelImpl : SpecialtiesModel {

    @Inject
    lateinit var db: AppDatabase

    init {
        App.dbComponent.inject(this)
    }

    override fun getDataFromServer(): Observable<ServerResponse> {
        return ApiService.api.loadData
    }

    override fun insertDataToDb(
            employees: ArrayList<Employee>,
            specialities: ArrayList<Specialty>,
            employeeSpecialtyJoin: ArrayList<EmployeeSpecialtyJoin>): Completable {

        return Completable.fromAction {
            db.employeeDao().deleteAll()
            db.specialityDao().deleteAll()
            db.employeeSpecialtyDao().deleteAll()

            insertEmployees(employees)
            insertSpecialties(specialities)
            insertEmployeesSpecialties(employeeSpecialtyJoin)
        }
    }

    private fun insertEmployees(employees: ArrayList<Employee>) {
        db.employeeDao().insertAll(employees)
    }

    private fun insertSpecialties(specialities: ArrayList<Specialty>) {
        db.specialityDao().insertAll(specialities)
    }

    private fun insertEmployeesSpecialties(employeeSpecialtyJoin: ArrayList<EmployeeSpecialtyJoin>) {
        db.employeeSpecialtyDao().insert(employeeSpecialtyJoin)
    }

    override fun getSpecialtiesFromDb(): Flowable<List<Specialty>> {
        return db.specialityDao().loadAll()
    }
}