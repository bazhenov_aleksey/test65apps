package ru.bazhenov.test65apps.view

import ru.bazhenov.test65apps.model.db.Employee

interface EmployeeDetailsView {

    fun showDetails(employee: Employee)
    fun getEmployeeId(): Int
    fun showSpecialties(specialties: String)
}