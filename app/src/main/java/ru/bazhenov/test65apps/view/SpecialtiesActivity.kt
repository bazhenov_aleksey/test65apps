package ru.bazhenov.test65apps.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_specialties.*
import ru.bazhenov.test65apps.R
import ru.bazhenov.test65apps.model.db.Specialty
import ru.bazhenov.test65apps.presenter.SpecialtiesPresenter
import ru.bazhenov.test65apps.presenter.SpecialtiesPresenterImpl

class SpecialtiesActivity : AppCompatActivity(), SpecialtiesView {


    val presenter: SpecialtiesPresenter = SpecialtiesPresenterImpl(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_specialties)

        presenter.onViewCreated()
    }

    override fun showSpecialties(list: List<Specialty>) {
        val adapter = SpecialtiesAdapter(list) {
            val intent = Intent(this@SpecialtiesActivity, EmployeesActivity::class.java)
            intent.putExtra(EmployeesActivity.SPECIALTY_ID, it.id)
            startActivity(intent)
        }
        rv_specialities.layoutManager = LinearLayoutManager(this)
        rv_specialities.adapter = adapter
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewStop()
    }
}
