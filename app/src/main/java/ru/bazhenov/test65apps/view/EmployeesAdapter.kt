package ru.bazhenov.test65apps.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_employee.view.*
import ru.bazhenov.test65apps.R
import ru.bazhenov.test65apps.model.db.Employee

class EmployeesAdapter(
        private val specialities: List<Employee>,
        private val itemClick: (Employee) -> Unit)
    : RecyclerView.Adapter<EmployeesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_employee, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(specialities[position])
    }

    override fun getItemCount() = specialities.size

    class ViewHolder(view: View, private val itemClick: (Employee) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bindView(e: Employee) {
            with(e) {
                itemView.first_name.text = firstName
                itemView.last_name.text = lastName
                itemView.age.text = age
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}
