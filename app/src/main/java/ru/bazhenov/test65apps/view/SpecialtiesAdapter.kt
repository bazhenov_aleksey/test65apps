package ru.bazhenov.test65apps.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_specialty.view.*
import ru.bazhenov.test65apps.R
import ru.bazhenov.test65apps.model.db.Specialty

class SpecialtiesAdapter(
        private val specialities: List<Specialty>,
        private val itemClick: (Specialty) -> Unit)
    : RecyclerView.Adapter<SpecialtiesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_specialty, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(specialities[position])
    }

    override fun getItemCount() = specialities.size

    class ViewHolder(view: View, private val itemClick: (Specialty) -> Unit)
        : RecyclerView.ViewHolder(view) {

        fun bindView(s: Specialty) {
            with(s) {
                itemView.specialty.text = name
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}
