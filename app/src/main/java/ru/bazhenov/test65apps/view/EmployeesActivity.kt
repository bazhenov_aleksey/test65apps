package ru.bazhenov.test65apps.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_employees.*
import ru.bazhenov.test65apps.R
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.presenter.EmployeesPresenter
import ru.bazhenov.test65apps.presenter.EmployeesPresenterImpl

class EmployeesActivity : AppCompatActivity(), EmployeesView {

    val presenter: EmployeesPresenter = EmployeesPresenterImpl(this)

    companion object {
        val SPECIALTY_ID = "SPECIALTY_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employees)

        presenter.onViewCreated()
    }

    override fun getSpecialtyId() = intent.getIntExtra(SPECIALTY_ID, 0)

    override fun showEmployees(employees: List<Employee>) {
        val adapter = EmployeesAdapter(employees) {
            val intent = Intent(this@EmployeesActivity, EmployeeDetailsActivity::class.java)
            intent.putExtra(EmployeeDetailsActivity.EMPLOYEE_ID, it.id)
            startActivity(intent)
        }
        rv_employees.layoutManager = LinearLayoutManager(this)
        rv_employees.adapter = adapter
    }
}
