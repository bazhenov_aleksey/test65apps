package ru.bazhenov.test65apps.view

import ru.bazhenov.test65apps.model.db.Employee

interface EmployeesView {
    fun showEmployees(employees: List<Employee>)
    fun getSpecialtyId(): Int
}