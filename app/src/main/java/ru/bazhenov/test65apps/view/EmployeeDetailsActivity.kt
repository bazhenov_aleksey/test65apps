package ru.bazhenov.test65apps.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_employee_details.*
import ru.bazhenov.test65apps.DateUtils
import ru.bazhenov.test65apps.R
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.presenter.EmployeeDetailsPresenter
import ru.bazhenov.test65apps.presenter.EmployeeDetailsPresenterImpl

class EmployeeDetailsActivity : AppCompatActivity(), EmployeeDetailsView {

    val presenter: EmployeeDetailsPresenter = EmployeeDetailsPresenterImpl(this)

    companion object {
        val EMPLOYEE_ID = "EMPLOYEE_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_details)

        presenter.onViewCreated()
    }

    override fun getEmployeeId() = intent.getIntExtra(EMPLOYEE_ID, 0)

    override fun showDetails(employee: Employee) {
        with(employee) {
            first_name.text = firstName
            last_name.text = lastName
            employee_birthday.text = DateUtils.getFormattedDate(birthday)
            if (avatarUrl != null && avatarUrl!!.isNotEmpty()) {
                Picasso.with(this@EmployeeDetailsActivity).load(avatarUrl).into(avatar)
            }
        }
    }

    override fun showSpecialties(specialties: String) {
        specialty.text = specialties
    }
}
