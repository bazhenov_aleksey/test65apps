package ru.bazhenov.test65apps.view

import ru.bazhenov.test65apps.model.db.Specialty

interface SpecialtiesView {
    fun showSpecialties(list: List<Specialty>)
    fun showError(message: String)
}