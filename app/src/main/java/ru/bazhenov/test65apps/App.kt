package ru.bazhenov.test65apps

import android.app.Application

import ru.bazhenov.test65apps.di.DaggerDbComponent
import ru.bazhenov.test65apps.di.DbComponent
import ru.bazhenov.test65apps.di.DbModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        dbComponent = buildDbComponent()
    }

    private fun buildDbComponent(): DbComponent {
        return DaggerDbComponent.builder()
                .dbModule(DbModule(this))
                .build()
    }

    companion object {

        lateinit var dbComponent: DbComponent
            private set
    }
}
