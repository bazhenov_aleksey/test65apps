package ru.bazhenov.test65apps

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class DateUtils {

    companion object {

        fun getFormattedDate(birthday: String?): String {
            var date: Date? = null
            try {
                date = getDateFormat(birthday).parse(birthday)
            } catch (e: Exception) {
                when (e) {
                    is ParseException, is NullPointerException -> {
                        return "-"
                    }
                    else -> throw e
                }
            }

            val newFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).format(date)
            return newFormat
        }

        fun getAge(birthday: String?): String {
            val date: Date?
            try {
                date = getDateFormat(birthday).parse(birthday)
            } catch (e: Exception) {
                when (e) {
                    is ParseException, is NullPointerException -> {
                        return "-"
                    }
                    else -> throw e
                }
            }

            val calendar = Calendar.getInstance()
            val nowYear = calendar.get(Calendar.YEAR)

            calendar.time = date
            val s = nowYear - calendar.get(Calendar.YEAR)
            return s.toString()
        }

        private fun getDateFormat(birthday: String?): DateFormat {
            val pattern = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}")
            val format: DateFormat?
            format = if (pattern.matcher(birthday).find()) {
                SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            } else {
                SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            }
            return format
        }
    }
}