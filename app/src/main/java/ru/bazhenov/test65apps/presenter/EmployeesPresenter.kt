package ru.bazhenov.test65apps.presenter

interface EmployeesPresenter {
    fun onViewCreated()
    fun onViewStop()
}
