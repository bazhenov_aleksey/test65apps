package ru.bazhenov.test65apps.presenter

interface EmployeeDetailsPresenter {
    fun onViewCreated()
    fun onViewStop()
}
