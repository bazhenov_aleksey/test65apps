package ru.bazhenov.test65apps.presenter

interface SpecialtiesPresenter {
    fun onViewCreated()
    fun onViewStop()
}
