package ru.bazhenov.test65apps.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.bazhenov.test65apps.model.SpecialtiesModel
import ru.bazhenov.test65apps.model.SpecialtiesModelImpl
import ru.bazhenov.test65apps.model.api.ServerResponse
import ru.bazhenov.test65apps.model.db.Employee
import ru.bazhenov.test65apps.model.db.EmployeeSpecialtyJoin
import ru.bazhenov.test65apps.model.db.Specialty
import ru.bazhenov.test65apps.view.SpecialtiesView

class SpecialtiesPresenterImpl(private var view: SpecialtiesView?,
                               var model: SpecialtiesModel? = SpecialtiesModelImpl()) : SpecialtiesPresenter {

    var disposable: CompositeDisposable = CompositeDisposable()

    override fun onViewCreated() {
        disposable.add(model!!.getDataFromServer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onSuccessLoading(it)
                }, {
                    view?.showError(it.message!!)
                })
        )
    }

    private fun onSuccessLoading(response: ServerResponse) {
        val list = response.employees
        for ((index, e) in list!!.withIndex()) {
            e.id = index
        }

        val employees = Employee.convertToDbObjects(list)
        val specialities = Specialty.convertToDbObjects(list)
        val employeeSpecialtyJoin = EmployeeSpecialtyJoin.convertToDbObjects(list)

        model!!.insertDataToDb(employees, specialities, employeeSpecialtyJoin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onSuccessSaving()
                }, {
                    view?.showError(it.message!!)
                })
    }

    private fun onSuccessSaving() {
        model!!.getSpecialtiesFromDb()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.showSpecialties(it)
                })
    }

    override fun onViewStop() {
        disposable.dispose()
        view = null
        model = null
    }
}
