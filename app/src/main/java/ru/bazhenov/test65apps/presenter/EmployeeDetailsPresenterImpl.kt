package ru.bazhenov.test65apps.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.bazhenov.test65apps.model.EmployeeDetailsModel
import ru.bazhenov.test65apps.model.EmployeeDetailsModelImpl
import ru.bazhenov.test65apps.view.EmployeeDetailsView

class EmployeeDetailsPresenterImpl(private var view: EmployeeDetailsView?,
                                   var model: EmployeeDetailsModel? = EmployeeDetailsModelImpl()) : EmployeeDetailsPresenter {

    var disposable: CompositeDisposable = CompositeDisposable()

    override fun onViewCreated() {
        disposable.add(model!!.getEmployeeDetails(view!!.getEmployeeId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.showDetails(it)
                })
        )
        disposable.add(model!!.getEmployeeSpecialties(view!!.getEmployeeId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val builder = StringBuilder()
                    for ((index, specialtyName) in it.withIndex()) {
                        builder.append(specialtyName.name)
                        if (it.size > 1 && index != it.lastIndex) {
                            builder.append(", ")
                        }
                    }
                    view?.showSpecialties(builder.toString())
                })
        )
    }

    override fun onViewStop() {
        disposable.dispose()
        view = null
        model = null
    }
}
