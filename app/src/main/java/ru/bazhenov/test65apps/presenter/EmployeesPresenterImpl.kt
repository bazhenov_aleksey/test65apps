package ru.bazhenov.test65apps.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.bazhenov.test65apps.model.EmployeesModel
import ru.bazhenov.test65apps.model.EmployeesModelImpl
import ru.bazhenov.test65apps.view.EmployeesView

class EmployeesPresenterImpl(private var view: EmployeesView?,
                             var model: EmployeesModel? = EmployeesModelImpl()) : EmployeesPresenter {

    var disposable: CompositeDisposable = CompositeDisposable()

    override fun onViewCreated() {
        disposable.add(model!!.getEmployees(view!!.getSpecialtyId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.showEmployees(it)
                })
        )
    }

    override fun onViewStop() {
        disposable.dispose()
        view = null
        model = null
    }
}
