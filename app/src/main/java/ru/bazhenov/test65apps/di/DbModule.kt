package ru.bazhenov.test65apps.di

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.bazhenov.test65apps.model.db.AppDatabase
import javax.inject.Singleton

@Module
class DbModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideDb(): AppDatabase {
        return Room.databaseBuilder(context,
                AppDatabase::class.java, "Sample.db")
                .build()
    }
}
