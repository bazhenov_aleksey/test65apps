package ru.bazhenov.test65apps.di

import dagger.Component
import ru.bazhenov.test65apps.model.EmployeeDetailsModelImpl
import ru.bazhenov.test65apps.model.EmployeesModelImpl
import ru.bazhenov.test65apps.model.SpecialtiesModelImpl
import javax.inject.Singleton

@Component(modules = arrayOf(DbModule::class))
@Singleton
interface DbComponent {
    fun inject(model: SpecialtiesModelImpl)
    fun inject(model: EmployeesModelImpl)
    fun inject(model: EmployeeDetailsModelImpl)
}
